Repozytorium zawiera skrypt napisany w bashu automatyzujący proces tworzenia pliku MakeFile.

## Poniżej są założena przyjęte przy tworzeniu skryptu:
1. wszystkie pliki źródłowe w podanym katalogu służą do utworzenia jednego programu wynikowego,
nazwa pliku wykonywalnego jest rdzen nazwy pliku zrodlowego, zawierajacego funkcje main,
2. w katalogu znajduje się podkatalog headers z lokalnymi plikami nagłówkowymi,
3. lokalne pliki nagłówkowe właczane są wybiórczo w niektórych plikach źródłowych w następujacy sposób: #include "cos.h"